//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
// Original authors
// Authors: Susanna Guatelli, susanna@uow.edu.au,
// Authors: Jeremy Davis, jad028@uowmail.edu.au
//
// This file modified by Gordon R Mackenzie, gm12172@bristol.ac.uk

#include "DetectorConstruction.hh"
#include "globals.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4SubtractionSolid.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4ChordFinder.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "SensitiveDetector.hh"
#include "G4SDManager.hh"
#include "G4UserLimits.hh"
#include "Randomize.hh"
#include "G4ThreeVector.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Trd.hh"
#include "G4NistManager.hh"

DetectorConstruction::DetectorConstruction(AnalysisManager *analysis_manager)
{
	analysis = analysis_manager;
}

DetectorConstruction::~DetectorConstruction()
{
}

G4VPhysicalVolume *DetectorConstruction::Construct()
{
	////////////////////////////////////////////////////////////////////////////
	// Material definitions

	// Get nist material manager
	G4NistManager *nist = G4NistManager::Instance();

	// Elements - basic and for device
	// G4Element and G4Material have different behaviour, so choose one or both.
	G4Element *H = nist->FindOrBuildElement("H"); // Hydrogen
	G4Element *B = nist->FindOrBuildElement("B"); // Boron
	G4Element *C = nist->FindOrBuildElement("C"); // Carbon
	G4Material *Cmat = nist->FindOrBuildMaterial("G4_C");
	G4Element *N = nist->FindOrBuildElement("N"); // Nitrogen
	G4Material *Nmat = nist->FindOrBuildMaterial("G4_N");
	G4Element *O = nist->FindOrBuildElement("O");		 // Oxygen
	G4Material *Cu = nist->FindOrBuildMaterial("G4_Cu"); // Copper
	G4Material *Au = nist->FindOrBuildMaterial("G4_Au"); // Gold

	// Elements - Steel 303 constituents, and silver for paint
	G4Material *Si = nist->FindOrBuildMaterial("G4_Si"); // Silicon
	G4Material *P = nist->FindOrBuildMaterial("G4_P");	 // Phosphorous
	G4Material *S = nist->FindOrBuildMaterial("G4_S");	 // Sulphur
	G4Material *Cr = nist->FindOrBuildMaterial("G4_Cr"); // Chromium
	G4Material *Mn = nist->FindOrBuildMaterial("G4_Mn"); // Manganese
	G4Material *Fe = nist->FindOrBuildMaterial("G4_Fe"); // Iron
	G4Material *Ni = nist->FindOrBuildMaterial("G4_Ni"); // Nickel
	G4Material *Ag = nist->FindOrBuildMaterial("G4_Ag"); // Silver

	// Simple compounds
	// Air - dry and 40 % humidity
	G4Material *DryAir = nist->FindOrBuildMaterial("G4_AIR");
	G4Material *WaterVap = nist->FindOrBuildMaterial("G4_WATER_VAPOR");
	G4Material *HumdAir = new G4Material("HumdAir", 1.178 * mg / cm3, 2);
	HumdAir->AddMaterial(DryAir, (100 - 0.01252) * perCent);
	HumdAir->AddMaterial(WaterVap, 0.01252 * perCent);

	// Diamond, electronic grade
	G4Material *diamond = new G4Material("diamond", 3.515 * g / cm3, 3);
	diamond->AddElement(B, 0.0000001 * perCent);
	diamond->AddElement(C, 99.9999998 * perCent);
	diamond->AddElement(N, 0.0000001 * perCent);

	// Silver epoxy paint and components. Technical data sheet included with
	// source code for recipe.
	// 1-ethoxy-propan-2-ol, C5O2H12
	G4Material *EOP2O = new G4Material("EOP2O", 0.898 * g / cm3, 3);
	EOP2O->AddElement(C, 5);
	EOP2O->AddElement(O, 2);
	EOP2O->AddElement(H, 12);
	// Ethanol
	G4Material *Ethanol = nist->FindOrBuildMaterial("G4_ETHYL_ALCOHOL");
	// Acetone
	G4Material *Acetone = nist->FindOrBuildMaterial("G4_ACETONE");
	// Ethyl acetate
	G4Material *EthylAcetate = new G4Material("EthylAcetate", 0.902 * g / cm3, 3);
	EthylAcetate->AddElement(C, 4);
	EthylAcetate->AddElement(O, 2);
	EthylAcetate->AddElement(H, 8);
	// Then finally, silver epoxy paint
	G4Material *SilverEpoxy = new G4Material("SilverEpoxy", 1.44 * g / cm3, 5);
	SilverEpoxy->AddMaterial(Ag, 47 * perCent);
	SilverEpoxy->AddMaterial(EOP2O, 21 * perCent);
	SilverEpoxy->AddMaterial(Ethanol, 21 * perCent);
	SilverEpoxy->AddMaterial(Acetone, 8 * perCent);
	SilverEpoxy->AddMaterial(EthylAcetate, 3 * perCent);

	//Define steel 303
	G4Material *Steel303 = new G4Material("Steel303", 8.03 * g / cm3, 10);
	Steel303->AddMaterial(Cmat, 0.1 * perCent);
	Steel303->AddMaterial(Cr, 18 * perCent);
	Steel303->AddMaterial(Mn, 2 * perCent);
	Steel303->AddMaterial(Si, 1 * perCent);
	Steel303->AddMaterial(P, 0.045 * perCent);
	Steel303->AddMaterial(S, 0.1825 * perCent);
	Steel303->AddMaterial(Ni, 9 * perCent);
	Steel303->AddMaterial(Nmat, 0.1 * perCent);
	Steel303->AddMaterial(Cu, 1 * perCent);
	Steel303->AddMaterial(Fe, (100 - 0.1 - 18 - 2 - 1 - 0.045 - 0.1825 - 9 - 0.1 - 1) * perCent); // i.e "all the rest"

	////////////////////////////////////////////////////////////////////////////
	// Envelope parameters
	//
	G4double env_sizeXY = 5 * mm, env_sizeZ = 2 * cm;
	G4Material *env_mat = HumdAir;

	// Option to switch on/off checking of volumes overlaps
	//
	G4bool checkOverlaps = true; // set to true when adding new shapes to check

	//
	// World
	//
	G4double world_sizeXY = 1.1 * env_sizeXY;
	G4double world_sizeZ = 1.1 * env_sizeZ;
	G4Material *world_mat = HumdAir;

	G4Box *solidWorld =
		new G4Box("World",													  //its name
				  0.5 * world_sizeXY, 0.5 * world_sizeXY, 0.5 * world_sizeZ); //its size

	G4LogicalVolume *logicWorld =
		new G4LogicalVolume(solidWorld, //its solid
							world_mat,	//its material
							"World");	//its name

	G4VPhysicalVolume *physWorld =
		new G4PVPlacement(0,			   //no rotation
						  G4ThreeVector(), //at (0,0,0)
						  logicWorld,	   //its logical volume
						  "World",		   //its name
						  0,			   //its mother  volume
						  false,		   //no boolean operation
						  0,			   //copy number
						  checkOverlaps);  //overlaps checking

	//
	// Envelope
	//
	G4Box *solidEnv =
		new G4Box("Envelope",											//its name
				  0.5 * env_sizeXY, 0.5 * env_sizeXY, 0.5 * env_sizeZ); //its size

	G4LogicalVolume *logicEnv =
		new G4LogicalVolume(solidEnv,	 //its solid
							env_mat,	 //its material
							"Envelope"); //its name

	new G4PVPlacement(0,			   //no rotation
					  G4ThreeVector(), //at (0,0,0)
					  logicEnv,		   //its logical volume
					  "Envelope",	   //its name
					  logicWorld,	   //its mother  volume
					  false,		   //no boolean operation
					  0,			   //copy number
					  checkOverlaps);  //overlaps checking

	////////////////////////////////////////////////////////////////////////////
	// Solid and logical volume definitions for items

	// Diamond shape
	G4double dia_dxdy = 4.5 * mm;
	G4double dia_dz = 0.5 * mm;

	G4Box *dia_solid =
		new G4Box("dia_solid", 0.5 * dia_dxdy,
				  0.5 * dia_dxdy, 0.5 * dia_dz); //its size

	// diamond cell 1 logical volume
	G4LogicalVolume *dia1_log =
		new G4LogicalVolume(dia_solid,			  //its solid
							diamond,			  //its material
							"dia1_log", 0, 0, 0); //its name
	// diamond cell 2 logical volume
	G4LogicalVolume *dia2_log =
		new G4LogicalVolume(dia_solid,			  //its solid
							diamond,			  //its material
							"dia2_log", 0, 0, 0); //its name
	// diamond cell 3 logical volume
	G4LogicalVolume *dia3_log =
		new G4LogicalVolume(dia_solid,			  //its solid
							diamond,			  //its material
							"dia3_log", 0, 0, 0); //its name
	// Top Contact
	G4double cont_dxdy = 3.5 * mm;
	G4double cont_dz = 100 * nm;

	G4Box *TC_solid =
		new G4Box("TC_solid", 0.5 * cont_dxdy,	   //its name
				  0.5 * cont_dxdy, 0.5 * cont_dz); //its size

	G4LogicalVolume *TC_log =
		new G4LogicalVolume(TC_solid,			//its solid
							Au,					//its material
							"TC_log", 0, 0, 0); //its name

	// Bottom Contact

	G4Box *BC_solid =
		new G4Box("BC_solid",										//its name
				  0.5 * cont_dxdy, 0.5 * cont_dxdy, 0.5 * cont_dz); //its size

	G4LogicalVolume *BC_log =
		new G4LogicalVolume(BC_solid,			//its solid
							Au,					//its material
							"BC_log", 0, 0, 0); //its name

	// top silver paint
	G4double paint_dz = 1000 * um;
	G4double paint_dr = 1.5 * mm;

	G4Cons *paint_solid = new G4Cons("paint_solid",
									 0,				 // base inner radius = 0 for cone
									 paint_dr,		 // baseouter radius
									 0,				 // top inner radius = 0 for cone
									 0.1 * paint_dr, // top outer radius
									 0.5 * paint_dz, //half height in z
									 0,				 // starting phi angle in radians
									 2. * M_PI);	 //angle of segment

	G4LogicalVolume *paint_log = new G4LogicalVolume(paint_solid,			//its solid
													 SilverEpoxy,			//its material
													 "paint_log", 0, 0, 0); //its name

	// inter-cell silver paint
	G4double dab_dz = 100 * um;
	G4double dab_dr = 1.5 * mm;

	G4Tubs *dab_solid = new G4Tubs("dab_solid",
								   0,
								   dab_dr,
								   0.5 * dab_dz,
								   0,
								   2. * M_PI);
	G4LogicalVolume *dab_log = new G4LogicalVolume(dab_solid,
												   SilverEpoxy,
												   "dab_log", 0, 0, 0);

	// wire
	G4double wire_dz = dia_dxdy * 0.5;
	G4double wire_dr = 50 * um;
	G4RotationMatrix *WireRotation = new G4RotationMatrix();
	WireRotation->rotateX(90. * deg);

	G4Tubs *wire_solid = new G4Tubs("wire_solid",
									0,
									wire_dr,
									0.5 * wire_dz,
									0,
									2. * M_PI);
	G4LogicalVolume *wire_log = new G4LogicalVolume(wire_solid,
													Cu,
													"wire_log", 0, 0, 0);

	//pedestal pin
	G4double pin_dz = 4 * mm;
	G4double pin_dr = 0.8 * mm;

	G4Tubs *pin_solid = new G4Tubs("pin_solid",
								   0,
								   pin_dr,
								   0.5 * pin_dz,
								   0,
								   2. * M_PI);
	G4LogicalVolume *pin_log = new G4LogicalVolume(pin_solid,
												   Steel303,
												   "pin_log", 0, 0, 0);

	//pedestal gold plating
	G4double plating_dz = pin_dz + 4 * um;
	G4double plating_dr = pin_dr + 2 * um;
	G4Tubs *PlatingOuter = new G4Tubs("PlatingOuter",
									  0,
									  plating_dr,
									  0.5 * plating_dz,
									  0,
									  2. * M_PI);
	G4SubtractionSolid *plating_solid = new G4SubtractionSolid("plating_solid", PlatingOuter, pin_solid);
	G4LogicalVolume *plating_log = new G4LogicalVolume(plating_solid,
													   Au,
													   "plating_log", 0, 0, 0);
	////////////////////////////////////////////////////////////////////////////
	// Position definitions
	G4ThreeVector dia1_pos = G4ThreeVector(0, 0, 0);								 // diamond 1
	G4ThreeVector T_pos1 = dia1_pos + G4ThreeVector(0, 0, 0.5 * (dia_dz + cont_dz)); // top contact to diamond 1
	G4ThreeVector B_pos1 = dia1_pos - G4ThreeVector(0, 0, 0.5 * (dia_dz + cont_dz)); // bottom contact to diamond 1

	G4ThreeVector dab_pos1 = T_pos1 + G4ThreeVector(0, 0, 0.5 * (cont_dz + dab_dz)); // silver paint between diamonds 1 and 2

	G4ThreeVector B_pos2 = dab_pos1 + G4ThreeVector(0, 0, 0.5 * (cont_dz + dab_dz)); // bottom contact to diamond 2
	G4ThreeVector dia2_pos = B_pos2 + G4ThreeVector(0, 0, 0.5 * (dia_dz + cont_dz)); // diamond 2
	G4ThreeVector T_pos2 = dia2_pos + G4ThreeVector(0, 0, 0.5 * (dia_dz + cont_dz)); // top contact to diamond 2

	G4ThreeVector dab_pos2 = T_pos2 + G4ThreeVector(0, 0, 0.5 * (cont_dz + dab_dz)); // silver paint between diamonds 2 and 3

	G4ThreeVector B_pos3 = dab_pos2 + G4ThreeVector(0, 0, 0.5 * (cont_dz + dab_dz)); // bottom contact to diamond 3
	G4ThreeVector dia3_pos = B_pos3 + G4ThreeVector(0, 0, 0.5 * (dia_dz + cont_dz)); // diamond 3
	G4ThreeVector T_pos3 = dia3_pos + G4ThreeVector(0, 0, 0.5 * (dia_dz + cont_dz)); // top contact to diamond 3

	G4ThreeVector paint_pos = T_pos3 + G4ThreeVector(0, 0, 0.5 * (paint_dz + cont_dz));				// silver epoxy paint
	G4ThreeVector wire_pos = paint_pos + G4ThreeVector(0, 0.5 * wire_dz, 0.5 * paint_dz + wire_dr); // central copy of copper wire

	G4ThreeVector pin_pos = G4ThreeVector(0, 0, -0.5 * (dia_dz + plating_dz) - cont_dz); // steel SMA pin and its gold plating

	// Offset vector to move the whole setup
	G4ThreeVector offset = G4ThreeVector(0, 0, -5 * mm);

	// Offset the positions (this would be better as a loop)
	dia1_pos = dia1_pos + offset;
	T_pos1 = T_pos1 + offset;
	B_pos1 = B_pos1 + offset;

	dab_pos1 = dab_pos1 + offset;

	dia2_pos = dia2_pos + offset;
	T_pos2 = T_pos2 + offset;
	B_pos2 = B_pos2 + offset;

	dab_pos2 = dab_pos2 + offset;

	dia3_pos = dia3_pos + offset;
	T_pos3 = T_pos3 + offset;
	B_pos3 = B_pos3 + offset;

	paint_pos = paint_pos + offset;
	wire_pos = wire_pos + offset;

	pin_pos = pin_pos + offset;
	////////////////////////////////////////////////////////////////////////////
	// Physical volumes via PVPlacement

	// CELL 1
	// diamond 1
	new G4PVPlacement(0,			  //no rotation
					  dia1_pos,		  //at position
					  dia1_log,		  //its logical volume
					  "dia1_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  0,			  //copy number
					  checkOverlaps); //overlaps checking

	// Top contact for diamond 1
	new G4PVPlacement(0,			  //no rotation
					  T_pos1,		  //at position
					  TC_log,		  //its logical volume
					  "TC_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  0,			  //copy number
					  checkOverlaps); //overlaps checking

	// Bottom contact for diamond 1
	new G4PVPlacement(0,			  //no rotation
					  B_pos1,		  //at position
					  BC_log,		  //its logical volume
					  "BC_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  0,			  //copy number
					  checkOverlaps); //overlaps checking

	// CELL 2
	// diamond 2
	new G4PVPlacement(0,			  //no rotation
					  dia2_pos,		  //at position
					  dia2_log,		  //its logical volume
					  "dia2_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  0,			  //copy number
					  checkOverlaps); //overlaps checking

	// Top contact for diamond 2
	new G4PVPlacement(0,			  //no rotation
					  T_pos2,		  //at position
					  TC_log,		  //its logical volume
					  "TC_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  1,			  //copy number
					  checkOverlaps); //overlaps checking

	// Bottom contact for diamond 2
	new G4PVPlacement(0,			  //no rotation
					  B_pos2,		  //at position
					  BC_log,		  //its logical volume
					  "BC_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  1,			  //copy number
					  checkOverlaps); //overlaps checking

	// CELL 3
	// diamond 3
	new G4PVPlacement(0,			  //no rotation
					  dia3_pos,		  //at position
					  dia3_log,		  //its logical volume
					  "dia3_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  0,			  //copy number
					  checkOverlaps); //overlaps checking

	// Top contact for diamond 3
	new G4PVPlacement(0,			  //no rotation
					  T_pos3,		  //at position
					  TC_log,		  //its logical volume
					  "TC_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  2,			  //copy number
					  checkOverlaps); //overlaps checking

	// Bottom contact for diamond 3
	new G4PVPlacement(0,			  //no rotation
					  B_pos3,		  //at position
					  BC_log,		  //its logical volume
					  "BC_phys",	  //its name
					  logicEnv,		  //its mother  volume
					  false,		  //no boolean operation
					  2,			  //copy number
					  checkOverlaps); //overlaps checking
	// CONNECTIONS AND HOUSING
	// Silver epoxy paint
	// top paint
	new G4PVPlacement(0,			  // no rotation
					  paint_pos,	  //position
					  paint_log,	  //its logical volume
					  "paint_phys",	  // its name
					  logicEnv,		  // its mother volume
					  false,		  // no boolean operator
					  0,			  // copy number
					  checkOverlaps); //overlaps checking
	// inter-cell paint dabs
	new G4PVPlacement(0,			  // no rotation
					  dab_pos1,		  //position
					  dab_log,		  //its logical volume
					  "dab_phys",	  // its name
					  logicEnv,		  // its mother volume
					  false,		  // no boolean operator
					  0,			  // copy number
					  checkOverlaps); //overlaps checking
	new G4PVPlacement(0,			  // no rotation
					  dab_pos2,		  //position
					  dab_log,		  //its logical volume
					  "dab_phys",	  // its name
					  logicEnv,		  // its mother volume
					  false,		  // no boolean operator
					  1,			  // copy number
					  checkOverlaps); //overlaps checking

	// Twisted copper wire modelled as five identical copper wires varying only
	// in position, so 5 PVPlacements
	new G4PVPlacement(WireRotation,	  // wire is rotated
					  wire_pos,		  // position
					  wire_log,		  // loval volume
					  "wire_phys",	  // name
					  logicEnv,		  // mother volume
					  false,		  // no boolean operator
					  0,			  // copy number
					  checkOverlaps); // overlaps checking

	new G4PVPlacement(WireRotation,
					  wire_pos + G4ThreeVector(2 * wire_dr, 0, 0),
					  wire_log,
					  "wire_phys",
					  logicEnv,
					  false,
					  1,
					  checkOverlaps);

	new G4PVPlacement(WireRotation,
					  wire_pos + G4ThreeVector(4 * wire_dr, 0, 0),
					  wire_log,
					  "wire_phys",
					  logicEnv,
					  false,
					  2,
					  checkOverlaps);

	new G4PVPlacement(WireRotation,
					  wire_pos - G4ThreeVector(2 * wire_dr, 0, 0),
					  wire_log,
					  "wire_phys",
					  logicEnv,
					  false,
					  3,
					  checkOverlaps);

	new G4PVPlacement(WireRotation,
					  wire_pos - G4ThreeVector(4 * wire_dr, 0, 0),
					  wire_log,
					  "wire_phys",
					  logicEnv,
					  false,
					  4,
					  checkOverlaps);

	//pedestal pin
	new G4PVPlacement(0,			  // no rotation
					  pin_pos,		  // position
					  pin_log,		  // logical volume
					  "pin_phys",	  // name
					  logicEnv,		  // mother volume
					  false,		  // no booleavan operator
					  0,			  // copy number
					  checkOverlaps); // overlaps checking

	//pedestal gold plating
	new G4PVPlacement(0,			  // no rotation
					  pin_pos,		  // position (same as pin)
					  plating_log,	  // logical volume
					  "plating_phys", // name
					  logicEnv,		  // mother volume
					  false,		  // no booleaven operation
					  0,			  // copy number
					  checkOverlaps); // overlaps checking

	return physWorld;
}

void DetectorConstruction::ConstructSDandField()
{
	SensitiveDetector *SD = new SensitiveDetector("SD", "DetectorHitsCollection", analysis);
	G4SDManager::GetSDMpointer()->AddNewDetector(SD);
	SetSensitiveDetector("dia3_log", SD);
}
