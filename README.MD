# triplecellmono

## Introduction
This simulation is adapted from the radioprotection example that ships with
GEANT4. It simulates a triple-celled diamond gammavoltaic device (DGV), as
 described in my PhD thesis, "A Diamond Gammavoltaic Cell" 
 (University of Bristol, 2021). It is built to simulate monoenergetic photons
 and measure the energy deposited into the DGV crystal.

## General use
The example this code is adapted from is the radioprotection example by Susanna 
Guatelli and Jeremy Davis, Centre For Medical Radiation Physics (CMRP), 
University of Wollongong, NSW, Australia. General useage instructions written by
them can be found in the TripleCellMono-source folder.

## Specific use
There is one source folder and two build folders. The test build folder is for
development, the build folder is the one pulled to the main computer for
running. This code is based on singlecellmono, which can be found 
[here](https://bitbucket.org/RobbieMackenzie/singlecellmono/src/main/). 
In each run, only one of the three cells is "turned on" in the sense that is a 
sensitive volume for collecting energy deposition data. This must be toggled by 
altering the detectorconstruction.cc file in TripleCellMono-source .
 In the build folder there are two bash scripts for batch running:

1. Benchmark.sh
This runs the simulation using a range of thread counts (default is 3 to 20),
batching the simulation at each thread count to run for a range of photon
energies (default is 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000 keV). The
scripts times the execution of the program and outputs the time to
benchmark_results.txt.
2. BatchRun.sh
This is the main run script for the simulation. It runs it for a range of photon
energies (default 1 - 2000 keV in 1 keV steps). Output is collated in
CollectedData.csv, where the output takes the form

Photon Energy | Edep/Hit | Edep/Hit Error | Hits | Hits Error | Total Edep | Total Edep Error

There is also Edep data (where Edep is recorded for each hit) for each photon
energy in a separate file, which has the format Edep_[Photon Energy].csv

## Data
There is data from previous runs in folders in the TripleCellMono-build folder.
The data used in my thesis can be found in the three folders starting Run... . 
The run number corresponds to the cell number as noted in the thesis.