#!/bin/bash
# This is a benchmarking script. It runs the GEANT4 batch several times, once
# per number of threads between 3 and 20, and times it, sending the time
# reading to a file.

for n in {3..6}
    do
    sed -i "24 s/threads=.*/threads=$n/" BatchRun.sh
    sed -i "47 s/pRunManager->SetNumberOfThreads(.*/pRunManager->SetNumberOfThreads($n);/" ../TripleCellMono-source/radioprotection.cc
    make -j12
    (time . BatchRun.sh) > /dev/null 2>> benchmark_results.txt
    done
